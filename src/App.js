import React from 'react';
import './App.css';
import { SHOW, HIDE } from './constants/buttons';

const ShowHideButton = (props) => {
  let { isVisible, onClick } = props;
  return (
    <button onClick={onClick} className="show_hide_button">
      { isVisible ? HIDE: SHOW }
    </button>
  )
}

const EmbeddedBlog = () => {
  return (
    <iframe 
      title="The Cloudshine Blog" 
      src="https://blog.thecloudshine.net" 
      className="embedded_blog"
    />
  )
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false
    }
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick = () => {
    this.setState(state => ({
      isVisible: !state.isVisible
    }));
  }
  render () {
    return (
      <div className="App">
        <header className="App-header">
          <ShowHideButton onClick={this.handleClick} isVisible={this.state.isVisible} />
          {this.state.isVisible &&
            <EmbeddedBlog />
          }
        </header>
      </div>
    );
  }
}

export default App;
